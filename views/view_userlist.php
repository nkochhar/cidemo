<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>User List</title>

    <style type="text/css">

        body {
            background-color: #fff;
            margin: 40px;
            font: 13px/20px normal Helvetica, Arial, sans-serif;
            color: #4F5155;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            border-bottom: 1px solid #D0D0D0;
            font-size: 19px;
            font-weight: normal;
            margin: 0 0 14px 0;
            padding: 14px 15px 10px 15px;
        }

        H3.message {
            text-align: center
        }

        form li {
            list-style: none;
        }

        code {
            font-family: Consolas, Monaco, Courier New, Courier, monospace;
            font-size: 12px;
            background-color: #f9f9f9;
            border: 1px solid #D0D0D0;
            color: #002166;
            display: block;
            margin: 14px 0 14px 0;
            padding: 12px 10px 12px 10px;
        }

        #body{
            margin: 0 15px 0 15px;
        }

        p.footer{
            text-align: right;
            font-size: 11px;
            border-top: 1px solid #D0D0D0;
            line-height: 32px;
            padding: 0 10px 0 10px;
            margin: 20px 0 0 0;
        }

        #container{
            margin: 10px;
            border: 1px solid #D0D0D0;
            -webkit-box-shadow: 0 0 8px #D0D0D0;
        }
    </style>
</head>
<body>
<h1>User List</h1>
<p>Here are all the users currently registered:</p>

<!-- This is the most ghetto table ever -->
<table border=1 cellspacing=2 cellpadding=2>
    <tr>
        <td>id</td>
        <td>username</td>
        <td>first name</td>
        <td>last name</td>
        <td>email</td>
        <td>reg date</td>
        <td>activation date</td>
    </tr>
    <?php
    foreach ($users->result_array() as $row)
    {
        echo '<tr>';
        echo '<td>'.$row['id'].'</td>';
        echo '<td>'.$row['username'].'</td>';
        echo '<td>'.$row['first_name'].'</td>';
        echo '<td>'.$row['last_name'].'</td>';
        echo '<td>'.$row['email'].'</td>';
        echo '<td>'.$row['reg_date'].'</td>';
        echo '<td>'.$row['activation_date'].'</td>';
        echo '</tr>';
    }
    ?>
</table>
<br/>
<?=anchor(base_url().'user/register/', 'User Registration Form');?>


</body>
</html>