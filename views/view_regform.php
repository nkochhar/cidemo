<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>User Registration</title>

	<style type="text/css">

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

    H3.message {
        text-align: center
    }

    form li {
        list-style: none;
    }

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>
<h3 class='message'>
    <?=$message?>
</h3>
<h1>User Registration</h1>
<p>Please fill in the details below:</p>

<?=form_open(base_url() . 'user/register'); ?>

<?php
    $username = array(
        'name'      =>      'username',
        'id'        =>      'username',
        'value'     =>      set_value('username')
    );
    $first_name = array(
        'name'      =>      'first_name',
        'id'        =>      'first_name',
        'value'     =>      set_value('first_name')
    );
    $last_name = array(
        'name'      =>      'last_name',
        'id'        =>      'last_name',
        'value'     =>      set_value('last_name')
    );
    $email = array(
        'name'      =>      'email',
        'id'        =>      'email',
        'value'     =>      set_value('email')
    );
    $password = array(
        'name'      =>      'password',
        'id'        =>      'password',
        'value'     =>      ''
    );
    $password_confirm = array(
        'name'      =>      'password_confirm',
        'id'        =>      'password_confirm',
        'value'     =>      ''
    );

?>

<ul>
    <li>
        <label>Username</label>
        <div>
            <?=form_input($username); ?>
        </div>
    </li>
    <li>
        <label>First Name</label>
        <div>
            <?=form_input($first_name); ?>
        </div>
    </li>
    <li>
        <label>Last Name</label>
        <div>
            <?=form_input($last_name); ?>
        </div>
    </li>
    <li>
        <label>Email</label>
        <div>
            <?=form_input($email); ?>
        </div>
    </li>
    <li>
        <label>Password</label>
        <div>
            <?=form_password($password); ?>
        </div>
    </li>
    <li>
        <label>Confirm Password</label>
        <div>
            <?=form_password($password_confirm); ?>
        </div>
    </li>
    <li>
        <?=validation_errors(); ?>
    </li>
    <li>
        <div>
            <?=form_submit(array('name' => 'register'), "Register"); ?>
        </div>
    </li>
</ul>

<?= form_close(); ?>
<br/>
<?=anchor(base_url(), 'User List');?>

</body>
</html>