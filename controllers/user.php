<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    function User()
    {
        parent::__construct();

        $this->load->model('User_model');
    }

	function index()
	{
        $query = $this->User_model->dump_user_table();

        $this->load->view('view_userlist', array('users' => $query));
	}

    function register()
    {
        $this->set_validation_rules();

        if ($this->form_validation->run() == FALSE)
        {
            // Show the registration form (again)
            $this->load->view('view_regform', array('message' => ''));
        }
        else
        {
            // Process the user registration
            $username = $this->input->post('username');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $pw = $this->input->post('password');



            $activation_code = $this->User_model->register_user($username, $pw, $email, $first_name, $last_name);

            $this->send_activation_email($email, $activation_code, TRUE);

        }

    }

    function activate()
    {
        $activation_code = $this->uri->segment(3);
        if ($activation_code == '')
        {
            // No code; you don't need to be here
            $this->load->view('view_regform', array('message' => ''));
        }
        else
        {
            $result = $this->User_model->activate_user($activation_code);
            if ($result)
            {
                // Successfully activated, direct to login if it existed
                // For now just send back to registration form with a message
                $this->load->view('view_regform', array('message' => "User account successfully activated."));
            }
            else
            {
                // Activation unsuccessful, send back to reg form with an error
                $this->load->view('view_regform', array('message' => "Unable to activate user account.  Either the account does not exist or is already activated."));
            }
        }
    }

    private function send_activation_email($email, $activation_code, $debug=FALSE)
    {
        // TODO: This is not fully implemented

        $this->load->library('email');
        $this->email->from('is3ggp@gmail.com');
        $this->email->to($email);
        $this->email->subject('Account Activation');

        $body = 'Thank you for registering!  Please click ' . anchor('http://localhost:8888/user/activate/' . $activation_code, 'here') . ' to activate your account.';

        if ($debug)
        {
            // Temporary glue page to complete activation workflow
            // link will send users to $this->activate
            echo "For demonstration/testing purposes:  ";
            echo $body;
        }
        else
        {
            $this->email->message($body);
            $this->email->send();
            // User needs to check email and click the link to continue

        }


    }

    private function set_validation_rules()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]|max_length[20]|alpha_dash|strtolower|callback_username_is_valid|xss_clean');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|alpha|max_length[20]|xss_clean');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|alpha|max_length[20]|xss_clean');
        $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|callback_email_is_valid|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric|min_length[6]|max_length[16]|xss_clean');
        $this->form_validation->set_rules('password_confirm', 'Password Confirmation', 'required|matches[password]|xss_clean');

        $this->form_validation->set_message('username_is_valid', '%s entered is already in use.');
        $this->form_validation->set_message('email_is_valid', '%s entered is already in use.');

    }

    function username_is_valid($username)
    {
        return $this->User_model->field_is_unique('username', $username);
    }

    function email_is_valid($email)
    {
        return $this->User_model->field_is_unique('email', $email);
    }

}
