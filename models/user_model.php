<?php

class User_model extends CI_Model {

    function User_model()
    {
        parent::__construct();

    }

    function register_user($username, $password, $email, $first_name, $last_name)
    {
        $pw_hash = sha1($password);

        $this->load->helper('string');
        $activation_code = random_string('alnum', 16);

        $this->db->set('username', $username);
        $this->db->set('password', "HEX('".$pw_hash."')", FALSE);
        $this->db->set('email', $email);
        $this->db->set('first_name', $first_name);
        $this->db->set('last_name', $last_name);
        $this->db->set('activation_code', $activation_code);
        $this->db->set('reg_date', 'NOW()', FALSE);

        $this->db->insert('users');

        return $activation_code;

    }

    function activate_user($activation_code)
    {
        $this->db->select('id')->from('users')->where('activation_code', $activation_code)->where('activation_date IS NULL', NULL, false);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
        {
            $row = $query->row();
            $this->db->set('activation_date', 'NOW()', FALSE);
            $this->db->where('id', $row->id);
            $this->db->update('users');

            return TRUE;
        }
        else
        {
            // Either the user account is already activated or cannot be found
            return FALSE;
        }

    }

    function dump_user_table()
    {
        $query = $this->db->get('users');
        return $query;
    }

    function field_is_unique($field_name, $value)
    {
        $query = $this->db->get_where('users', array($field_name => $value));

        if ($query->num_rows() > 0)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }

    }
}